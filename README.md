# Twitch Chat Repeater

The base version of this script where written in 60 minutes.
Its dirty. It does what it has to.
Nothing more. (and it has probably many bugs)

## Disclaimer

I am in no way responsible for you either (ab)using this script, nor any damage that comes through trolls or similar.

## Installation / Usage

If you after all you have read above still want to use it, here is how:

**Either** wait until i get around to compile a binary for your OS **OR** compile from source.

## Running it

1. First run it once to generate the config file in the current dir.
2. Edit the config file to match your needs (and fill out the required info, see below)
3. Run the program again
4. ???
5. Profit

## Compiling from source

- Get the rust compiler (should be at least rustc 1.26)
- Get the openssl libs (libssl-dev)
- Clone this repo and `cd` into the folder.
- Run `cargo build --release`

Congrats, the folder `target/release/chat_repeater` now has your binary.
Now follow the steps "running it" above

## The config file

```yaml
---
# Twitch specific configs
twitch:
  # Your oauth token, you get it from https://twitchapps.com/tmi/
  oauth: "oauth:AAABBBCCCDDDEEEFFFGGGHHHIIIJJJ"
  # Your LOWERCASE username (you use to log into twitch)
  username: "your_user_name"
# Channel and "trigger" configs
channels:
  # The channel to listen to
  - channel: "#some_channel"
    # A list of all users and their triggers
    trigger:
      # User config
      user_you_trust:
        # Each element has 2 parts:
        # to: this regex is used to "check" if we should respond to the message
        # respond: The text to respond. if its set to null (yaml: ~) the whole message text will be used
        # if its not null but a string, all capture groups found in the text will be expanded appropialty
        # for syntax, look at the docs here: https://docs.rs/regex/1.0.1/regex/
        - to: "^!vote [0-9]$"
          respond: ~
        - to: "^!raffle (\\w{1,10})$"
          respond: "!enter $1"
      # You can specify a "generic" user per channel,
      # this will match any username and tries to apply the regex to their messages
      # BUT: Specific User "matches" take precedence before generic matches
      "*":
        - to: "^VoHiYo ?$"
          respond: "Hello o/"
      other_user_you_trust:
        - on: "^!sr https://youtube.com/watch?q=\\w+$"
          respond: ~
  - channel: "#other_channel"
    # You can also specify no triggers at all
    trigger: {}
```

**NOTE:** The channel and/or user `"*"` is a generic user. It matches all joined channels or all users in a channel.
**But** Specific users are <u>always</u> evaluated before the generic ones.
Searching stops when the first match is found.

## Known "Bugs"

- No ratelimiting: If the "target" spamms, you do too.

## Thanks

- Thanks to [SashaDraga](https://twitter.com/Sashadraga) for giving me ideas and enduring my endless "look at what i did"
- Thanks to [moxian#8121](https://discordapp.com) for troubleshooting twitch's irc with me.
