use chrono;
use fern;
use irc::{
    client::{Client, IrcClient},
    proto::{message::Tag, Command},
};
use log;
use rand::prelude::*;
use std::{
    collections::HashMap,
    io,
    iter::FromIterator,
    sync::mpsc::{channel, Sender},
    thread,
    time::Duration,
};
use MESSAGES_PER_SECONDS;

/// Spawns the outbound thread for client messages.
/// This is used to enforce ratelimits and avoid blocking in the worker-thread(s)
pub fn spawn_sender_thread(client: IrcClient) -> Sender<Command> {
    let (tx, rx) = channel::<Command>();
    thread::Builder::new()
        .name(String::from("Outbound Thread"))
        .spawn(move || {
            while let Ok(cmd) = rx.recv() {
                trace!("Outbound message: {:?}", cmd);
                if let Command::PRIVMSG(_, ref a) = cmd {
                    if a.len() > 50 {
                        warn!(
                            "We will send the message but it will probably not appear ! (length: \
                             {})",
                            a.len()
                        );
                        warn!("Message: {:?}", a);
                    }
                }
                match client.send(cmd) {
                    Ok(_ok) => {
                        #[cfg(not(feature = "reduced-verbose-log"))]
                        trace!("MSG: OK: {:?}", _ok);
                    },
                    Err(e) => {
                        error!("Error sending msg");
                        error!("{:#?}", e);
                    },
                }
                thread::sleep(Duration::from_millis(
                    (MESSAGES_PER_SECONDS.1 * 1000) / MESSAGES_PER_SECONDS.0,
                ));
            }
        })
        .expect("Unable to spawn sender thread");
    tx
}

pub fn spawn_delay_thread(sender_thread: Sender<Command>) -> Sender<(u64, u64, Command)> {
    let (tx, rx) = channel::<(u64, u64, Command)>();

    thread::Builder::new()
        .name(String::from("Delay Thread"))
        .spawn(move || {
            let mut rng = thread_rng();
            while let Ok((lower, upper, cmd)) = rx.recv() {
                trace!("Delayed message: {:?} (DELAY [{}, {}])", cmd, lower, upper);
                thread::sleep(Duration::from_millis(rng.gen_range(lower, upper + 1)));
                if sender_thread.send(cmd).is_err() {
                    error!("Error");
                    return;
                }
            }
        })
        .expect("Unable to spawn delay thread");

    tx
}

/// Given a string, extracts the "username" from it. its expected to be in a irc-conform format
pub fn find_twitch_name_prefx(s: Option<String>) -> String {
    if let Some(prefx) = s {
        let s = prefx.split('@').collect::<Vec<_>>();
        if s.len() == 2 {
            let sx = s[0].split('!').collect::<Vec<_>>();
            if sx.len() == 2 {
                String::from(sx[0])
            } else {
                String::from(s[0])
            }
        } else {
            String::from("[SOMEBODY]")
        }
    } else {
        String::from("[UNKNOWN]")
    }
}

/// Logging data,
/// all things concerning logging are in here.
pub struct Logging;
impl Logging {
    /// Initialize fern for logging with the default configuration
    pub fn init(verbosity: u64) -> Result<(), fern::InitError> {
        use fern::colors::{Color, ColoredLevelConfig};

        let app_verbosity = match verbosity {
            0 => log::LevelFilter::Error,
            1 => log::LevelFilter::Warn,
            2 => log::LevelFilter::Info,
            3 => log::LevelFilter::Debug,
            _ => log::LevelFilter::Trace,
        };

        let coloring = ColoredLevelConfig::default()
            .debug(Color::BrightBlue)
            .trace(Color::BrightMagenta)
            .info(Color::BrightCyan)
            .warn(Color::BrightYellow)
            .error(Color::BrightRed);

        ::fern::Dispatch::new()
            .format(move |out, message, record| {
                out.finish(format_args!(
                    "{level:>14} {time:>8} [{target}]: {message}",
                    time = chrono::Local::now().format("%j-%T"),
                    target = record.target(),
                    level = coloring.color(record.level()).to_string(),
                    message = message
                ))
            })
            .level(log::LevelFilter::Info)
            .level_for(env!("CARGO_PKG_NAME"), app_verbosity)
            .chain(io::stdout())
            .apply()?;
        trace!("Logging Initialized");
        Ok(())
    }
}

pub fn tags_into_hashmap(tags: &[Tag]) -> HashMap<String, String> {
    HashMap::from_iter(
        tags.into_iter()
            .filter(|i| i.1 != None && i.1 != Some("".to_owned()))
            .map(|Tag(l, r)| (l.clone(), r.clone().unwrap())),
    )
}

pub fn stringy_bool(s: &str) -> bool {
    match s {
        "y" | "yes" | "1" | "true" => true,
        _ => false,
    }
}

macro_rules! ioe {
    ($e:expr) => {
        ::std::io::Error::new(::std::io::ErrorKind::Other, $e);
    };
}

macro_rules! tx {
    ($tox:ident => $data:expr) => {
        match $tox.send($data) {
            Ok(_) => (),
            Err(_) => {
                return Err(IrcError::from(io::Error::new(
                    io::ErrorKind::Other,
                    String::from("Channel closed !"),
                )))
            },
        }
    };
}

#[allow(unused_macros)]
macro_rules! trace_struct_multiline {
    ($e:expr) => {
        trace_log_multiline!(format!("{:#?}", $e));
    };
}

#[allow(unused_macros)]
macro_rules! trace_log_multiline {
    ($e:expr) => {
        for l in $e.split("\n") {
            trace!("{}", l);
        }
    };
}
