extern crate chrono;
extern crate ctrlc;
extern crate fern;
extern crate irc;
extern crate parking_lot;
extern crate rand;
extern crate regex;
extern crate serde;
extern crate serde_yaml;

#[macro_use]
extern crate log;

#[macro_use]
extern crate serde_derive;

#[macro_use]
pub mod utils;
pub mod config;
pub mod handler;

use handler::{Counter, RegexTrigger};
use irc::{
    client::prelude::{Client, Config as IrcConfig, IrcReactor},
    proto::{CapSubCommand, Command},
};
use parking_lot::{Mutex, RwLock};
use regex::Regex;
use std::{
    collections::{HashMap, HashSet},
    fs::File,
    io::Write,
    path::Path,
    sync::{
        atomic::{AtomicBool, Ordering as AtomicOrdering},
        Arc,
    },
    thread,
};
use utils::{spawn_delay_thread, spawn_sender_thread, Logging};

const MESSAGES_PER_SECONDS: (u64, u64) = (20, 32);
const PYRAMID_DEFAULT_HEIGHT: usize = 3;

fn main() {
    // Initialize Logging
    let lglvl = ::std::env::var("REPEATER_LOG_LEVEL")
        .unwrap_or_else(|_| "2".to_owned())
        .parse::<u64>()
        .unwrap_or(2);
    Logging::init(lglvl).expect("Unable to init logging");
    let config = config::Config::load("conf.yaml").expect("Unable to load config");

    info!("Checking channels");
    let chnre = Regex::new("^(?:#\\w+|\\*)$").expect("Invalid regex");

    let config_user_name = config.twitch.username.clone();

    let mut remap: HashMap<String, HashMap<String, Vec<RegexTrigger>>> = HashMap::new();
    let mut chanvec = Vec::new();
    for channel in config.channels.clone() {
        // For each channel configured, if matches
        if chnre.is_match(&channel.name) {
            // Add it to the channels to join
            if channel.name != "*" {
                chanvec.push(channel.name.clone());
            }

            // Create channel->Vec<to/respond> map
            let mut tm = HashMap::new();
            for (name, triggers) in channel.trigger {
                let mut res = Vec::new();
                for reg in triggers {
                    match Regex::new(&reg.to) {
                        Ok(r) => {
                            let rt = RegexTrigger(r, reg.respond);
                            debug!("REGEX {}/{} -> {:?}", channel.name, name, rt);
                            res.push(rt);
                        },
                        Err(why) => warn!(
                            "I+nvalid regex: {} & {} => `{}` ({:?})",
                            channel.name, name, reg.to, why
                        ),
                    }
                }
                tm.insert(name, res);
            }
            remap.insert(channel.name, tm);
        } else {
            warn!("Invalid channel `{}`. skipping...", channel.name);
        }
    }

    let oauth = config.twitch.oauth.clone();
    let name = config.twitch.username.clone();
    let irc_cfg = IrcConfig {
        server: Some("irc.chat.twitch.tv".to_owned()),
        port: Some(443),
        use_ssl: Some(true),
        ..IrcConfig::default()
    };

    trace!("Creating atomics");
    let atomic_stop = Arc::new(AtomicBool::new(false));
    let atomic_stats = Arc::new(Mutex::new(Counter::new()));

    // Variables moved to thread
    let atomic_stop_all = Arc::clone(&atomic_stop);
    let stats = Arc::clone(&atomic_stats);
    // Variables moved to thread
    match thread::Builder::new()
        .name("Irc Worker Thread".to_string())
        .spawn(move || {
            let mut reactor = IrcReactor::new().expect("Unable to create IrcReactor");
            let client = reactor
                .prepare_client_and_connect(&irc_cfg)
                .expect("Unable to initialize client");

            info!("Authenticating");
            debug!("Aut pass");
            client
                .send(Command::Raw("PASS".to_owned(), vec![oauth], None))
                .expect("Unable to send PASS");

            debug!("Auth nick");
            client
                .send(Command::Raw("NICK".to_owned(), vec![name], None))
                .expect("Unable to send NICK");

            info!("Sending caps");
            // debug!("No need, we only need the PRIVMSG command");
            // Sending Channel Capability
            debug!("CAP Tags");
            client
                .send(Command::CAP(
                    None,
                    CapSubCommand::REQ,
                    Some(":twitch.tv/tags".to_owned()),
                    None,
                ))
                .expect("Unable to set Channel Tag Capabilies");

            info!("Joining all channels");
            for chn in chanvec {
                debug!("JOIN {}", chn);
                client
                    .send(Command::JOIN(chn.clone(), None, None))
                    .unwrap_or_else(|_| panic!("Unable to join channel {}", &chn))
            }

            let send = spawn_sender_thread(client.clone());
            let delay_snd = spawn_delay_thread(send.clone());

            let auth_users = {
                let mut tmp = config.twitch.auth_users.unwrap_or_else(Vec::default);
                tmp.push(config_user_name.clone());
                tmp.push("heapunderflow".to_owned());

                tmp.dedup();

                tmp.into_iter().collect::<HashSet<String>>()
            };
            let skip_same_user = config.twitch.repeat_skip_owner.unwrap_or(true);

            // List of all channels we are mod in
            // as this is only used for the ">inb4 pyramid [emote]" command
            let mod_cache = RwLock::new(HashSet::<String>::new());

            reactor.register_client_with_handler(client, move |_, msg| {
                handler::handle_message(
                    msg,
                    &remap,
                    &send,
                    &delay_snd,
                    &stats,
                    &mod_cache,
                    skip_same_user,
                    &config_user_name,
                    &auth_users,
                )
            });

            debug!("Irc started");
            error!("Were now alive and kicking !");
            if let Err(e) = reactor.run() {
                error!("A error occured while running the client:");
                error!("{:#?}", e);
                atomic_stop_all.store(true, AtomicOrdering::Relaxed);
            }
        }) {
        Ok(_) => (),
        Err(e) => {
            error!("Unable to start IRC WORKER");
            error!("{:?}", e);
            std::process::exit(0x02);
        },
    }

    info!("Registering CTRL-C (SIGINT) handler");

    let astop = Arc::clone(&atomic_stop);
    match ctrlc::set_handler(move || {
        info!("Setting stop flag...");
        astop.store(true, AtomicOrdering::Relaxed);
    }) {
        Ok(_) => (),
        Err(e) => {
            error!("FATAL ERROR: Unable to register CTRL-C handler !");
            error!("{:?}", e);
            atomic_stop.store(true, AtomicOrdering::Relaxed);
        },
    };

    loop {
        if atomic_stop.load(AtomicOrdering::Relaxed) {
            warn!("Stopping, saving stats !");

            {
                let stats = SavedStats {
                    timestamp: chrono::Local::now(),
                    stats:     atomic_stats.lock().clone(),
                };

                let serstats =
                    serde_yaml::to_string(&stats).expect("Holy fuck something gone wrong HARD");

                if !Path::new("stats.d").exists() {
                    match std::fs::create_dir("stats.d") {
                        Ok(_) => (),
                        Err(e) => {
                            error!("Hell, i have no permissions !");
                            error!("{:?}", e);
                            error!("Still, take this:");
                            println!("{}", serstats);
                            std::process::exit(0x03);
                        },
                    }
                }

                let fname = format!("stats.d/STATS_{}.yaml", stats.timestamp.to_rfc3339());
                let mut f = match File::create(&fname) {
                    Ok(fx) => fx,
                    Err(e) => {
                        error!("Hell, i have no permissions !");
                        error!("{:?}", e);
                        error!("Still, take this:");
                        println!("{}", serstats);
                        std::process::exit(0x03);
                    },
                };

                write!(f, "{}", serstats).expect("ok, u fucked up");
            }

            std::process::exit(0x00);
        }
        thread::sleep(::std::time::Duration::from_millis(250));
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct SavedStats {
    pub timestamp: chrono::DateTime<chrono::Local>,
    pub stats:     Counter,
}
