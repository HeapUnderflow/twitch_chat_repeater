use irc::{
    error::IrcError,
    proto::{Command, Message},
};
use parking_lot::{Mutex, RwLock};
use regex::Regex;
use std::{
    collections::{HashMap, HashSet},
    io,
    sync::mpsc::Sender,
};
use utils::find_twitch_name_prefx;
use PYRAMID_DEFAULT_HEIGHT;

pub type Counter = HashMap<String, HashMap<String, u64>>;
pub type UserTriggerMap = HashMap<String, HashMap<String, Vec<RegexTrigger>>>;
#[derive(Debug)]
pub struct RegexTrigger(pub Regex, pub Option<String>);

pub fn handle_message<S: ::std::hash::BuildHasher>(
    cmsg: Message,
    trigger: &UserTriggerMap,
    outbound: &Sender<Command>,
    delayed_outbound: &Sender<(u64, u64, Command)>,
    stats: &Mutex<Counter>,
    mod_cache: &RwLock<HashSet<String, S>>,
    skip_same_user: bool,
    config_user_name: &str,
    auth_users: &HashSet<String, S>,
) -> Result<(), IrcError> {
    match cmsg.command {
        Command::PING(a, b) => tx!(outbound => Command::PONG(a, b)),
        Command::PRIVMSG(chn, msg) => {
            let mut match_found = false;
            let username = find_twitch_name_prefx(cmsg.prefix);
            let mut stat_v = stats.lock();

            if auth_users.contains(&username) && msg.starts_with(">inb4") {
                let sp = msg.split(' ').collect::<Vec<_>>();
                if sp.len() > 1 {
                    // ---- STATS
                    if sp[1] == "stats" {
                        info!("Printing stats");

                        if let Some(stat) = stat_v.get(&chn) {
                            debug!("We have stats");

                            let mut stat_list = Vec::new();
                            for (k_trigger, v_count) in stat {
                                trace!("PUSH: {:?} = {}", k_trigger, v_count);
                                stat_list.push((k_trigger, v_count));
                            }

                            stat_list.sort_unstable_by(|(_, a), (_, b)| a.cmp(b));

                            let select = stat_list
                                .iter()
                                .rev()
                                .take(5)
                                .map(|(re, am)| format!("{} x{}", re, am))
                                .collect::<Vec<_>>()
                                .join(", ");

                            tx!(delayed_outbound => (1000, 1000, Command::PRIVMSG(
                                chn,
                                format!(
                                    "(top5): {}",
                                    select
                                ),
                            )));
                        } else {
                            debug!("We didnt have stats");
                            tx!(delayed_outbound => (1000, 1000, Command::PRIVMSG(chn.clone(), "I dont have any stats for ya today mate !".to_owned())));
                        }

                        return Ok(());
                    }

                    // --- PYRAMID
                    if sp[1] == "pyramid" && sp.len() > 2 {
                        let text = sp[2];
                        let len = if sp.len() > 3 {
                            sp[3].parse::<usize>().unwrap_or(PYRAMID_DEFAULT_HEIGHT) + 1
                        } else {
                            PYRAMID_DEFAULT_HEIGHT + 1
                        };

                        info!("Printing a [{}] pyramid with `{}`", len, text);

                        let mut lines_to_send: Vec<String> = Vec::with_capacity(len * 2 - 1);

                        for i in 0..len {
                            if i == 0 {
                                continue;
                            }
                            let mut line = String::new();
                            for _ in 0..i {
                                line.push_str(&format!("{} ", text));
                            }
                            lines_to_send.push(line);
                        }

                        for i in (0..(len - 1)).rev() {
                            if i == 0 {
                                continue;
                            }
                            let mut line = String::new();
                            for _ in 0..i {
                                line.push_str(&format!("{} ", text));
                            }
                            lines_to_send.push(line);
                        }

                        for line in lines_to_send {
                            tx!(outbound => Command::PRIVMSG(chn.clone(), line));
                        }
                    }
                }
            }

            if skip_same_user && config_user_name == username {
                trace!("This user matches with message user, skipping");
                return Ok(());
            }
            // --------- SPECIFIC CHANNEL SECTION ---------------

            if let Some(channel_trigger) = trigger.get(&chn) {
                // --------- SPECIFIC CHANNEL / SPECIFIC USER --------

                if let Some(user_trigger) = channel_trigger.get(&username) {
                    match_found = handle_trigger(
                        &delayed_outbound,
                        &mut stat_v,
                        user_trigger,
                        &msg,
                        &chn,
                        &username,
                        false,
                        false,
                    )?;
                }

                // --------- SPECIFIC CHANNEL / GENERIC USER --------

                if !match_found {
                    if let Some(generic_trigger) = channel_trigger.get("*") {
                        match_found = handle_trigger(
                            &delayed_outbound,
                            &mut stat_v,
                            generic_trigger,
                            &msg,
                            &chn,
                            &username,
                            false,
                            true,
                        )?;
                    }
                }
            }

            // ---------- GENERIC CHANNEL SECTION -------------

            if !match_found {
                if let Some(generic_channel_trigger) = trigger.get("*") {
                    // ---------- GENERIC CHANNEL / SPECIFIC USER -----------

                    if let Some(g_user_trigger) = generic_channel_trigger.get(&username) {
                        match_found = handle_trigger(
                            &delayed_outbound,
                            &mut stat_v,
                            g_user_trigger,
                            &msg,
                            &chn,
                            &username,
                            true,
                            false,
                        )?;
                    }

                    // ---------- GENERIC CHANNEL / GENERIC USER -----------

                    if !match_found {
                        if let Some(g_generic_trigger) = generic_channel_trigger.get("*") {
                            let _ = handle_trigger(
                                &delayed_outbound,
                                &mut stat_v,
                                g_generic_trigger,
                                &msg,
                                &chn,
                                &username,
                                true,
                                true,
                            )?;
                        }
                    }
                }
            }
        },
        Command::Raw(c, mut chn_list, ..) => {
            if "USERSTATE" == c.as_str() {
                let chn = match chn_list.pop() {
                    Some(s) => s,
                    None => "****".to_owned(),
                };
                if let Some(tags) = &cmsg.tags {
                    let tagmap = ::utils::tags_into_hashmap(&tags);

                    let zero = String::from("0");
                    let is_mod = ::utils::stringy_bool(tagmap.get("mod").unwrap_or(&zero));
                    // We dont care if the "default" is invalid or not, as we only compare to mod anyway
                    let type_mod = tagmap.get("user-type").unwrap_or(&zero) == "mod";
                    if is_mod && type_mod {
                        let mut mc = mod_cache.write();
                        mc.insert(chn);
                    }
                }
            }
        },
        op => trace!("> Unknown op: {:?}", op),
    }

    Ok(())
}

pub fn handle_trigger(
    outbound: &Sender<(u64, u64, Command)>,
    stats: &mut Counter,
    triggers: &[RegexTrigger],
    ms: &str,
    cs: &str,
    us: &str,
    generic_chn: bool,
    generic_usr: bool,
) -> Result<bool, IrcError> {
    #[cfg(not(feature = "reduced-verbose-log"))]
    trace_log_multiline!(format!(
        "ARGS handle_trigger\noutbound   : {:?}\ntrigger    : {:?}\nms         : {:?}\ncs         \
         : {:?}\nus         : {:?}\ngeneric_chn: {:?}\ngeneric_usr: {:?}",
        outbound, triggers, ms, cs, us, generic_chn, generic_usr
    ));
    'trigger_loop: for trigger_re in triggers {
        // Check if the trigger matches
        let caps = trigger_re.0.captures(ms);
        if caps.is_none() {
            continue 'trigger_loop;
        }
        // "Unpack" the match
        let caps = caps.expect("Sanity check failed for caps.unwrap()");

        info!(
            "MATCH {gen_chn}{chn} & {gen_usr}{usr} > `{msg}`",
            chn = &cs,
            usr = us,
            msg = &ms,
            gen_chn = if generic_chn { "*" } else { "" },
            gen_usr = if generic_usr { "*" } else { "" }
        );

        let mut count = stats
            .entry(cs.to_owned())
            .or_insert_with(HashMap::new)
            .entry(trigger_re.0.as_str().to_owned())
            .or_insert(0);
        *count += 1;
        // Expand the message, if the user has given a replacement string
        let response = match trigger_re.1.clone() {
            None => ms.to_owned(),
            Some(ref respond_string) => {
                // expand the string with our captures
                let mut expand_target = String::with_capacity(respond_string.len());
                caps.expand(respond_string, &mut expand_target);
                expand_target
            },
        };

        debug!("Responding with `{}`", response);
        tx!(outbound => (1000, 3000, Command::PRIVMSG(cs.to_owned(), response)));
        return Ok(true);
    }

    Ok(false)
}
