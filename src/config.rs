use serde_yaml;
use std::{
    collections::HashMap,
    env,
    fs::{self, File},
    io::{self, Write},
    path::Path,
};

/// Base configuration
#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Config {
    pub twitch:   TwitchConfig,
    pub channels: Vec<ChannelConfig>,
}
/// Twitch specific documentation
#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct TwitchConfig {
    /// oauth token
    pub oauth: String,
    /// login name
    pub username: String,
    /// users allowed to use commands
    pub auth_users: Option<Vec<String>>,
    pub repeat_skip_owner: Option<bool>,
}

/// Channel config
#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct ChannelConfig {
    /// Name of the channel (prefixed with `#`)
    #[serde(rename = "channel")]
    pub name: String,
    /// List of triggers
    pub trigger: HashMap<String, Vec<TriggerConfig>>,
}

/// Trigger config
#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct TriggerConfig {
    /// Regex to respond to
    pub to: String,
    /// Optional respond format
    pub respond: Option<String>,
}

impl Default for Config {
    fn default() -> Config {
        Config {
            twitch:   TwitchConfig::default(),
            channels: vec![ChannelConfig::default()],
        }
    }
}

impl Default for TwitchConfig {
    fn default() -> TwitchConfig {
        TwitchConfig {
            oauth:             String::from("oauth:xxx"),
            username:          String::from("example"),
            auth_users:        None,
            repeat_skip_owner: None,
        }
    }
}

impl Default for ChannelConfig {
    fn default() -> ChannelConfig {
        ChannelConfig {
            name:    String::from("#example_channel"),
            trigger: HashMap::default(),
        }
    }
}

impl Default for TriggerConfig {
    fn default() -> TriggerConfig {
        TriggerConfig {
            to:      String::from("^$"),
            respond: None,
        }
    }
}

impl Config {
    // Load
    pub fn load(pth: &str) -> io::Result<Config> {
        trace!("Loading config from `{}`", pth);
        if !Path::new(pth).exists() {
            trace!("Config missing, recreating");
            let ncfg = Config::default();
            let d = match serde_yaml::to_string(&ncfg) {
                Ok(s) => s,
                Err(e) => {
                    trace!("Compiler fuckup");
                    return Err(ioe!(e));
                },
            };

            trace!("Writing config...");
            let mut f = File::create(&pth)?;
            write!(f, "{}", d)?;
            warn!("Created missing config !");
            return Err(ioe!("Config missing".to_owned()));
        }

        let d = fs::read_to_string(&pth)?;

        match serde_yaml::from_str::<Config>(&d) {
            Ok(mut o) => {
                if o.twitch.oauth.is_empty() {
                    match env::var("TWITCH_OAUTH_TOKEN") {
                        Ok(tk) => o.twitch.oauth = tk,
                        Err(e) => {
                            trace!("Missing oauth token, delegating error up");
                            return Err(ioe!(e));
                        },
                    }
                }
                Ok(o)
            },
            Err(e) => Err(ioe!(e)),
        }
    }
}
